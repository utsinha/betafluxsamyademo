let express = require("express")
let path = require("path")
var compression = require("compression")

// Importing required modules
// import Home from '../shared/Home';

// PORT for server
const PORT = process.env.PORT || 3000

// Calling expres server
let app = express()
app.use(compression())

// Setting static dir path
app.use(express.static("build"))

app.get("/", (req, res) => {
	res.sendFile(path.resolve("public", "index.html"))
})

// Server listening to portPORT
app.listen(PORT, () => {
	console.log("App is running on ", PORT)
})
