import React, { Component } from "react"
import "./Pages.css"
import Searchbar from "../Components/Searchbar/Searchbar"
import HeaderOptions from "../Components/HeaderOptions/HeaderOptions"
import Scorecard from "../Components/Scorecard/Scorecard"
import DropdownSelector from "../Components/DropdownSelector/DropdownSelector"
import SettingsIcon from "../images/settings_icon.png"
import DownloadIcon from "../images/download_icon.png"
import ScenarioCard from "../Components/ScenarioCard/ScenarioCard"

class Page1 extends Component {
	constructor(props) {
		super(props)
	}
	state = {
		ScorecardBottomData: {
			YTDVariance: {
				Actual: "80",
				Plan: "100",
				Variance: "-20"
			},
			PredictedVariance: {
				Actual: "80",
				Plan: "100",
				Variance: "20"
			},
			ForwardRisk: {
				ChartData: []
			},
			ForwardOpportunities: {
				ChartData: []
			}
		},
		ScenarioCardData: {
			DemandTrend: {
				Title: "Demand Trend - Decline",
				IsPositive: false,
				Alert: "HIGH",
				AlertEmotion: "Negative",
				Status: "ADDRESSED",
				StatusEmotion: "Positive",
				TableData: {
					Product: "Mountain Dew",
					Area: "North America",
					Dc: "Boston",
					PotentialDrivers: "Weather, Promotion",
					RollingvsPlan: "-20",
					RollingBias: "20"
				}
			},
			DemandOpportunity: {
				Title: "Demand Opportunity Weather",
				IsPositive: true,
				Alert: "HIGH",
				AlertEmotion: "Positive",
				Status: "PENDING",
				StatusEmotion: "Neutral",
				TableData: {
					Product: "Pepsi",
					Area: "North America",
					Dc: "Miami",
					PotentialDrivers: "Weather, Promotion",
					RollingvsPlan: "+20",
					RollingBias: "20"
				}
			},
			DemandRisk: {
				Title: "Demand Risk - Competitor Pricing",
				IsPositive: true,
				Alert: "MEDIUM",
				AlertEmotion: "Neutral",
				Status: "IGNORED",
				StatusEmotion: "Negative",
				TableData: {
					Product: "Mountain Dew",
					Area: "North America",
					Dc: "Boston",
					PotentialDrivers: "Weather, Promotion",
					RollingvsPlan: "-20",
					RollingBias: "20"
				}
			}
		}
	}

	// Lifecycle Hooks
	componentDidMount() {
		const ForwardRiskChartData = Array.from(Array(7).keys()).map((point) => {
			return this.getRandomInt(100)
		})

		const ForwardOpportunitiesChartData = Array.from(Array(7).keys()).map((point) => {
			return this.getRandomInt(100)
		})

		this.setState((prevState) => {
			return {
				...prevState,
				ScorecardBottomData: {
					...prevState.ScorecardBottomData,
					ForwardRisk: {
						ChartData: ForwardRiskChartData
					},
					ForwardOpportunities: {
						ChartData: ForwardOpportunitiesChartData
					}
				}
			}
		})
	}

	getRandomInt = (max) => {
		return Math.floor(Math.random() * Math.floor(max))
	}

	render() {
		const { ScorecardBottomData, ScenarioCardData } = this.state
		const {
			YTDVariance,
			PredictedVariance,
			ForwardRisk,
			ForwardOpportunities
		} = ScorecardBottomData
		const { DemandTrend, DemandOpportunity, DemandRisk } = ScenarioCardData

		return (
			<React.Fragment>
				<div className="PageContainer">
					<div className="PageWrapper">
						<div className="header">
							<Searchbar />
							<HeaderOptions />
						</div>

						<div className="TitleAndFilterContainer">
							<div style={{ color: "#9B9B9B", fontSize: "0.9rem" }}>
								Showing results for{" "}
								<span style={{ margin: "0 1rem", fontSize: "18px" }}>&rarr;</span>
								<span style={{ color: "#111", fontWeight: "800" }}>
									PEPSICO BRAND
								</span>
							</div>
							<div className="DropdownSelector_container">
								<DropdownSelector
									defaultValue="2 Weeks"
									label="Look Back"
									option={["2 Weeks", "4 Weeks", "6 Weeks"]}
								/>
								<DropdownSelector
									defaultValue="2 Weeks"
									label="Look Forward"
									option={["2 Weeks", "4 Weeks", "6 Weeks"]}
								/>
								<DropdownSelector
									defaultValue="3 Items"
									label="Show"
									option={["4 Items", "5 Items", "6 Items"]}
								/>
								<img
									src={SettingsIcon}
									alt="Settings Icon"
									className="SettingsIcon"
								/>
								<div className="DownloadButton">
									<img
										src={DownloadIcon}
										alt="Download Button"
										style={{ width: "1rem", marginRight: "0.5rem" }}
									/>
									<span style={{ marginTop: "0.1rem" }}>DOWNLOAD</span>
								</div>
							</div>
						</div>

						<div className="ScorecardsContainer">
							<Scorecard
								titleData="10%"
								titleDataLabel="YTD Variance"
								gain={false}
								hasChart={false}
								actual={this.state.ScorecardBottomData.YTDVariance.Actual}
								plan={this.state.ScorecardBottomData.YTDVariance.Plan}
								variance={this.state.ScorecardBottomData.YTDVariance.Variance}
							/>
							<Scorecard
								titleData="20%"
								titleDataLabel="Predited Variance"
								gain={false}
								hasChart={false}
								actual={this.state.ScorecardBottomData.PredictedVariance.Actual}
								plan={this.state.ScorecardBottomData.PredictedVariance.Plan}
								variance={this.state.ScorecardBottomData.PredictedVariance.Variance}
							/>
							<Scorecard
								titleData="$100k"
								titleDataLabel="Forward Risk"
								gain={false}
								hasChart={true}
								chartData={ForwardRisk.ChartData}
								chartLabel={"Risk"}
								chartLineColor="#d0021b"
								chartFillColor="rgba(208, 2, 27, .1)"
							/>
							<Scorecard
								titleData="$237k"
								titleDataLabel="Forward Opportunities"
								gain={true}
								hasChart={true}
								chartData={ForwardOpportunities.ChartData}
								chartLabel={"Opportunities"}
								chartLineColor="#21d394"
								chartFillColor="rgba(33, 211, 148, .1)"
							/>
						</div>

						<ScenarioCard
							title={this.state.ScenarioCardData.DemandTrend.Title}
							isPositive={this.state.ScenarioCardData.DemandTrend.IsPositive}
							alert={this.state.ScenarioCardData.DemandTrend.Alert}
							alertEmotion={this.state.ScenarioCardData.DemandTrend.AlertEmotion}
							status={this.state.ScenarioCardData.DemandTrend.Status}
							statusEmotion={this.state.ScenarioCardData.DemandTrend.StatusEmotion}
							product={this.state.ScenarioCardData.DemandTrend.TableData.Product}
							area={this.state.ScenarioCardData.DemandTrend.TableData.Area}
							dc={this.state.ScenarioCardData.DemandTrend.TableData.Dc}
							potentialDrivers={
								this.state.ScenarioCardData.DemandTrend.TableData.PotentialDrivers
							}
							rollingvsPlan={
								this.state.ScenarioCardData.DemandTrend.TableData.RollingvsPlan
							}
							rollingBias={
								this.state.ScenarioCardData.DemandTrend.TableData.RollingBias
							}
						/>

						<ScenarioCard
							title={this.state.ScenarioCardData.DemandOpportunity.Title}
							isPositive={this.state.ScenarioCardData.DemandOpportunity.IsPositive}
							alert={this.state.ScenarioCardData.DemandOpportunity.Alert}
							alertEmotion={
								this.state.ScenarioCardData.DemandOpportunity.AlertEmotion
							}
							status={this.state.ScenarioCardData.DemandOpportunity.Status}
							statusEmotion={
								this.state.ScenarioCardData.DemandOpportunity.StatusEmotion
							}
							product={
								this.state.ScenarioCardData.DemandOpportunity.TableData.Product
							}
							area={this.state.ScenarioCardData.DemandOpportunity.TableData.Area}
							dc={this.state.ScenarioCardData.DemandOpportunity.TableData.Dc}
							potentialDrivers={
								this.state.ScenarioCardData.DemandOpportunity.TableData
									.PotentialDrivers
							}
							rollingvsPlan={
								this.state.ScenarioCardData.DemandOpportunity.TableData
									.RollingvsPlan
							}
							rollingBias={
								this.state.ScenarioCardData.DemandOpportunity.TableData.RollingBias
							}
						/>

						<ScenarioCard
							title={this.state.ScenarioCardData.DemandRisk.Title}
							isPositive={this.state.ScenarioCardData.DemandRisk.IsPositive}
							alert={this.state.ScenarioCardData.DemandRisk.Alert}
							alertEmotion={this.state.ScenarioCardData.DemandRisk.AlertEmotion}
							status={this.state.ScenarioCardData.DemandRisk.Status}
							statusEmotion={this.state.ScenarioCardData.DemandRisk.StatusEmotion}
							product={this.state.ScenarioCardData.DemandRisk.TableData.Product}
							area={this.state.ScenarioCardData.DemandRisk.TableData.Area}
							dc={this.state.ScenarioCardData.DemandRisk.TableData.Dc}
							potentialDrivers={
								this.state.ScenarioCardData.DemandRisk.TableData.PotentialDrivers
							}
							rollingvsPlan={
								this.state.ScenarioCardData.DemandRisk.TableData.RollingvsPlan
							}
							rollingBias={
								this.state.ScenarioCardData.DemandRisk.TableData.RollingBias
							}
						/>
					</div>
				</div>
			</React.Fragment>
		)
	}
}

export default Page1
