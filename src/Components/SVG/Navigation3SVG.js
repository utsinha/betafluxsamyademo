import React from 'react'

const SvgIcon = (props) => {
  return(
    <svg width="22px" height="24.44px" viewBox="0 0 27 30" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>Group</title>
        <desc>Created with Sketch.</desc>
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Desktop" transform="translate(-47.000000, -337.000000)" stroke={props.stroke}>
                <g id="Group" transform="translate(47.000000, 337.000000)">
                    <polygon id="Path-2" points="0.777294479 7.54546202 14.194389 14.9775059 14.194389 28.5709875 0.777294479 20.9700172"></polygon>
                    <polyline id="Path-3" points="14.5000706 14.7986521 26.4575191 7.84813882 26.4575191 20.8802143 14.5000706 28.4343312"></polyline>
                    <polyline id="Path-7" points="0.428381412 7.32298739 13.3961983 0.524927374 26.3640152 7.32298739"></polyline>
                    <path d="M7.5,10.5 L19.5,3.92395738" id="Line" stroke-linecap="square"></path>
                </g>
            </g>
        </g>
    </svg>

  )
}

export default SvgIcon
