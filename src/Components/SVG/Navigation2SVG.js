import React from 'react'

const SvgIcon = (props) => {
  return(
    <svg width="22px" height="35.2px" viewBox="0 0 25 40" version="1.1" xmlns="http://www.w3.org/2000/svg">
        <title>Group 2</title>
        <desc>Created with Sketch.</desc>
        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
            <g id="Desktop" transform="translate(-46.000000, -238.000000)">
                <g id="Group-2" transform="translate(47.000000, 239.000000)">
                    <circle id="Oval" stroke={props.stroke} stroke-width="1.2" cx="11.5" cy="11.5" r="11.5"></circle>
                    <text id="$" font-family="Avenir-Medium, Avenir" font-size="17" font-weight="400" fill={props.fill}>
                        <tspan x="7" y="17">$</tspan>
                    </text>
                    <polyline id="Path-4" stroke={props.stroke}  stroke-width="1.2" points="0 38.5258903 7.74387095 31.6711603 14.5246223 36.9021827 23.2345314 29"></polyline>
                    <polyline id="Path-5" stroke={props.stroke}  points="17.8572779 29 23 29 23 33.9753164"></polyline>
                </g>
            </g>
        </g>
    </svg>
  )
}

export default SvgIcon
