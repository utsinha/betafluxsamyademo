import React, { Component } from "react"
import { NavLink } from "react-router-dom"
import "./Sidebar.css"
import Logo from "../../images/demo_logo.png"
import Navigation1 from "../SVG/Navigation1SVG"
import Navigation2 from "../SVG/Navigation2SVG"
import Navigation3 from "../SVG/Navigation3SVG"
import Navigation4 from "../SVG/Navigation4SVG"

class sidebar extends Component {
	state = {
		Navigation1_isActive: true,
		Navigation2_isActive: false,
		Navigation3_isActive: false,
		Navigation4_isActive: false
	}

	menuClickHandler = (e) => {
		const stateVariable = e.target.closest(".NavigationIcon").getAttribute("data-stateVariable")
		var originalState = {
			Navigation1_isActive: false,
			Navigation2_isActive: false,
			Navigation3_isActive: false,
			Navigation4_isActive: false
		}

		if (!this.state[stateVariable]) {
			this.setState((prevState) => {
				return {
					...originalState,
					[stateVariable]: true
				}
			})
		}
	}

	render() {
		return (
			<div className="Sidebar">
				<div className="LogoContainer">
					<img src={Logo} alt="Samya.AI" />
				</div>

				<NavLink to="/">
					<div
						className="NavigationIcon"
						data-name="Menu Item 1"
						data-stateVariable="Navigation1_isActive"
						onClick={this.menuClickHandler}
					>
						<Navigation1
							stroke={this.state.Navigation1_isActive ? "#F29043" : "#9B9B9B"}
						/>
						<div
							className={
								this.state.Navigation1_isActive ? "NavPointer" : "NavPointer Hidden"
							}
						></div>
					</div>
				</NavLink>

				<NavLink to="/Page2">
					<div
						className="NavigationIcon"
						data-name="Menu Item 2"
						data-stateVariable="Navigation2_isActive"
						onClick={this.menuClickHandler}
					>
						<Navigation2
							stroke={this.state.Navigation2_isActive ? "#F29043" : "#9B9B9B"}
							fill={this.state.Navigation2_isActive ? "#F29043" : "#9B9B9B"}
						/>
						<div
							className={
								this.state.Navigation2_isActive ? "NavPointer" : "NavPointer Hidden"
							}
						></div>
					</div>
				</NavLink>

				<NavLink to="/Page3">
					<div
						className="NavigationIcon"
						data-name="Menu Item 3"
						data-stateVariable="Navigation3_isActive"
						onClick={this.menuClickHandler}
					>
						<Navigation3
							stroke={this.state.Navigation3_isActive ? "#F29043" : "#9B9B9B"}
						/>
						<div
							className={
								this.state.Navigation3_isActive ? "NavPointer" : "NavPointer Hidden"
							}
						></div>
					</div>
				</NavLink>

				<NavLink to="/Page4">
					<div
						className="NavigationIcon"
						data-name="Menu Item 4"
						data-stateVariable="Navigation4_isActive"
						onClick={this.menuClickHandler}
					>
						<Navigation4
							fill={this.state.Navigation4_isActive ? "#F29043" : "#9B9B9B"}
						/>
						<div
							className={
								this.state.Navigation4_isActive ? "NavPointer" : "NavPointer Hidden"
							}
						></div>
					</div>
				</NavLink>
			</div>
		)
	}
}

export default sidebar
