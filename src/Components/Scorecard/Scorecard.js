import React from "react"
import { Line } from "react-chartjs-2"
import "./Scorecard.css"
import LossIcon from "../../images/scorecardLoss_icon.png"
import GainIcon from "../../images/scorecardGain_icon.png"

const Scorecard = (props) => {
	const { chartData: LineChartData, chartLineColor, chartLabel, chartFillColor } = props
	const variance = parseInt(props.variance)
	let ScorecardBottom

	if (!props.hasChart) {
		ScorecardBottom = (
			<div className="Bottom">
				<div className="ContentContainer">
					<span>ACTUAL</span>
					<span style={{ marginLeft: "auto", fontWeight: "600" }}>{props.actual}</span>
				</div>
				<div className="ContentContainer">
					<span>PLAN</span>
					<span style={{ marginLeft: "auto", fontWeight: "600" }}>{props.plan}</span>
				</div>
				<div className="ContentContainer">
					<span>VARIANCE</span>
					<span
						className={variance > 0 ? "Variance Positive" : "Variance Negative"}
						style={{ marginLeft: "auto", fontWeight: "600" }}
					>
						{props.variance}
					</span>
				</div>
			</div>
		)
	} else {
		const chartData = {
			labels: Array.from(Array(7).keys()).map((k) => `Day ${k + 1}`),
			datasets: [
				{
					label: "Risk/Opportunities",
					data: LineChartData,
					// fill: false,
					// tension: 1,
					pointBackgroundColor: chartLineColor,
					pointRadius: 0,
					pointStyle: "circle",
					datalabels: {
						display: false
					},
					borderColor: chartLineColor,
					borderWidth: 2,
					backgroundColor: chartFillColor
				}
			]
		}

		const chartOptions = {
			responsive: true,
			maintainAspectRatio: false,
			legend: {
				display: false
			},
			tooltips: {
				mode: "index",
				intersect: false,
				backgroundColor: "#444C5E",
				callbacks: {
					label: function(tooltipItem) {
						const { value } = tooltipItem
						return `${chartLabel} : $${value}k`
					}
				}
			},
			layout: {
				padding: {
					left: 30,
					right: 30,
					top: 20,
					bottom: 20
				}
			},
			scales: {
				// Y-axis stylings and rules
				yAxes: [
					{
						display: false
					}
				],
				// X-axis stylings and rules
				xAxes: [
					{
						display: false
					}
				]
			}
		}
		ScorecardBottom = (
			<div className="Bottom">
				<div
					style={{
						height: "100px",
						width: "100%",
						position: "relative"
					}}
				>
					<Line data={chartData} options={chartOptions} />
				</div>
			</div>
		)
	}

	return (
		<div className="Scorecard">
			<div className="Top">
				<div className="ContentContainer">
					<div class="Content">
						<h2>{props.titleData}</h2>
						<p>{props.titleDataLabel}</p>
					</div>
					<img
						src={props.gain ? GainIcon : LossIcon}
						alt={props.gain ? "Gain Icon" : "Loss Icon"}
					/>
				</div>
			</div>

			{ScorecardBottom}
		</div>
	)
}

export default Scorecard
