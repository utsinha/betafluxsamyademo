import React from "react"
import "./ScenarioCard.css"
import { Line } from "react-chartjs-2"

const ScenarioCard = (props) => {
	const getRandomInt = (max) => {
		return Math.floor(Math.random() * Math.floor(max))
	}

	const chartData = {
		labels: Array.from(Array(6).keys()).map((k) => `Point ${k + 1}`),
		datasets: [
			{
				label: "Plan",
				data: [40, 50, 60, 70, 80, 90, 100],
				fill: false,
				tension: 0,
				pointBackgroundColor: "blue",
				pointRadius: 0,
				pointStyle: "circle",
				datalabels: {
					display: false
				},
				borderColor: "blue",
				borderWidth: 2
			},
			{
				label: "Actual",
				data: [, , , 70, getRandomInt(50), getRandomInt(55), getRandomInt(90)],
				fill: false,
				tension: 0.1,
				pointBackgroundColor: "#555",
				pointRadius: 0,
				pointStyle: "circle",
				datalabels: {
					display: false
				},
				borderColor: "#555",
				borderWidth: 2
			},
			{
				label: "Samya AI",
				data: [getRandomInt(10), getRandomInt(20), getRandomInt(60), 70],
				fill: false,
				tension: 0,
				pointBackgroundColor: "orange",
				pointRadius: 0,
				pointStyle: "circle",
				datalabels: {
					display: false
				},
				borderColor: "orange",
				borderWidth: 2
			}
		]
	}

	const chartOptions = {
		responsive: true,
		maintainAspectRatio: false,
		legend: {
			display: false
		},
		tooltips: {
			mode: "index",
			intersect: false,
			backgroundColor: "#444C5E",
			callbacks: {
				label: function(tooltipItem, data) {
					var label = data.datasets[tooltipItem.datasetIndex].label || ""
					const { value } = tooltipItem
					return `${label} : $${value}k`
				}
			}
		},
		layout: {
			padding: {
				left: 0,
				right: 30,
				top: 20,
				bottom: 20
			}
		},
		scales: {
			// Y-axis stylings and rules
			yAxes: [
				{
					ticks: {
						display: false,
						beginAtZero: true,
						maxTicksLimit: 6
					},
					stacked: false,
					gridLines: {
						drawBorder: false,
						drawTicks: false,
						lineWidth: 1,
						zeroLineWidth: 1,
						zeroLineColor: "#ddd",
						zeroLineBorderDash: [4],
						borderDash: [4],
						zeroLineBorderDashOffset: [4]
					}
				}
			],
			// X-axis stylings and rules
			xAxes: [
				{
					display: false,
					stacked: false
				}
			]
		}
	}

	return (
		<div
			className={
				props.isPositive
					? "ScenarioCardContainer Positive"
					: "ScenarioCardContainer Negative"
			}
		>
			<div className="ScenarioCardTitleContainer">
				<h4>
					{props.title} <span className={props.alertEmotion}>{props.alert}</span>
				</h4>
				<div className={"ScenarioCardStatus " + props.statusEmotion}>{props.status}</div>
			</div>

			<div className="ScenarioCard">
				<div className="Table">
					<ul>
						<li>
							<span className="attribute">Product</span>
							<span className="value">{props.product}</span>
						</li>
						<li>
							<span className="attribute">Area</span>
							<span className="value">{props.area}</span>
						</li>
						<li>
							<span className="attribute">DC</span>
							<span className="value">{props.dc}</span>
						</li>
						<li>
							<span className="attribute">Potential Drivers</span>
							<span className="value">{props.potentialDrivers}</span>
						</li>
						<li>
							<span className="attribute">Rolling Variance Vs Plan</span>
							<span className="value">{props.rollingvsPlan}</span>
						</li>
						<li>
							<span className="attribute">Rolling Bias</span>
							<span className="value">{props.rollingBias}</span>
						</li>
					</ul>
				</div>

				<div className="HighlightAndChart">
					<h4>Predicted Variance</h4>
					<div className="Highlights">
						<div
							className={
								props.isPositive ? "Item First Positive" : "Item First Negative"
							}
						>
							<h2>20%</h2>
							<p>Predicted Variance</p>
						</div>
						<div
							className={
								props.isPositive ? "Item Second Positive" : "Item Second Negative"
							}
						>
							<h2>$200k</h2>
							<p>Amount</p>
						</div>
						<div
							className={
								props.isPositive ? "Item Third Positive" : "Item Third Negative"
							}
						>
							<h2>40</h2>
							<p>Volume Units</p>
						</div>
					</div>
					<div
						clasName="ChartContainer"
						style={{
							marginTop: "2rem",
							marginRight: "3rem",
							display: "flex",
							alignItems: "flex-end"
						}}
					>
						<div
							style={{
								height: "200px",
								width: "100%",
								position: "relative"
							}}
						>
							<Line data={chartData} options={chartOptions} />
						</div>
						<ul className="ChartContainerLegend">
							<li className="ChartContainerLegend_Item">
								<span
									className="ChartContainerLegend_Color"
									style={{ background: "blue" }}
								></span>
								<div className="ChartContainerLegend_Title">PLAN</div>
							</li>
							<li className="ChartContainerLegend_Item">
								<span
									className="ChartContainerLegend_Color"
									style={{ background: "orange" }}
								></span>
								<div className="ChartContainerLegend_Title">ACTUAL</div>
							</li>
							<li className="ChartContainerLegend_Item">
								<span
									className="ChartContainerLegend_Color"
									style={{ background: "#555" }}
								></span>
								<div className="ChartContainerLegend_Title">SAMYA.AI FORECAST</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	)
}

export default ScenarioCard
