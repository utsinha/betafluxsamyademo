import React from 'react'
import './HeaderOptions.css'
import NotificationIcon from '../../images/notification_icon.png'

const HeaderOptions = () => {
  return(
    <div className='HeaderOptions'>
      <div>Aug 27, 2019</div>
      <div style={{fontSize:'10px', margin:'23px 0 auto 0'}}>&#9679;</div>
      <div>00 : 30 : 19</div>
      <img className='NotificationIcon' src={NotificationIcon} alt='Notification Icon' />
      <img className='ProfileImage' src='https://img2.thejournal.ie/inline/2470754/original/?width=428&version=2470754' alt='Profile Piture' />
    </div>
  )
}

export default HeaderOptions
