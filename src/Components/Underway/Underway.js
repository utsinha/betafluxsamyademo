import React from "react"
import "./Underway.css"
import Illustration from "../../images/underway_illustration.png"

const Underway = (props) => {
	return (
		<div className="UnderwayContent">
			<img src={Illustration} alt="Illustration showing data visualization dashboard" />

			<h3>Hang Around!</h3>
			<p>This Machine's brewing some good business insights</p>
		</div>
	)
}

export default Underway
