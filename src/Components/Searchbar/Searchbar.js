import React from 'react'
import './Searchbar.css'
import SearchIcon from '../../images/search_icon.png'

const Searchbar = () => {

  return(

    <div className='SearchbarContainer'>
      <img src={SearchIcon} alt='Search Icon' />
      <input type='text' placeholder='Search...' />
    </div>
    
  )

}

export default Searchbar
