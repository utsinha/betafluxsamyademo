import React, { Component } from "react";
import "./DropdownSelector.css";

class DropdownSelector extends Component {

  constructor(props) {
    super(props);
    this.dropdownButtonRef = React.createRef(null);
  }

  state = {
    isOpen: false
  };

  componentDidUpdate() {
    if (this.state.isOpen) {
      document.addEventListener("mousedown", this.handleClickOutside);
    } else {
      document.removeEventListener("mousedown", this.handleClickOutside);
    }
  }

  componentWillUnmount() {
    document.removeEventListener("mousedown", this.handleClickOutside);
  }

  handleClickOutside = e => {
    if (this.dropdownButtonRef.current.contains(e.target)) {
      return false;
    }

    this.toggleDropDownHandler();
  };

  toggleDropDownHandler = state => {
    if (typeof state == "boolean") {
      this.setState({
        isOpen: state
      });
    } else {
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
  };

  render() {
    return (
      <div
        className="DropdownSelector"
        data-label={this.props.label}
        onClick={this.toggleDropDownHandler}
        ref={this.dropdownButtonRef}
      >
        <span>{this.props.defaultValue}</span>
        <div className={this.state.isOpen ? "OptionList" : "OptionList Hidden"}>
          <ul>
          {this.props.option.map(item => {
            return <li>{item}</li>;
          })}
          </ul>
        </div>
      </div>
    )
  }
}

export default DropdownSelector;
