import React from 'react';
import './App.css';
import Sidebar from './Components/Sidebar/Sidebar'
import Router from './Router'

function App() {
  return (
    <div className="App">
      <Router>
      <Sidebar />
      </Router>
    </div>
  );
}

export default App;
