import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import Page1 from './Pages/Page1'
import Page2 from './Pages/Page2'
import Page3 from './Pages/Page3'
import Page4 from './Pages/Page4'

const Router = (props) => {
  const {children} = props
  return(
    <BrowserRouter>
    {children}
      <Switch>
        <Route exact path="/" component={Page1} />
        <Route exact path="/page2" component={Page2} />
        <Route exact path="/page3" component={Page3} />
        <Route exact path="/page4" component={Page4} />
      </Switch>
    </BrowserRouter>
  )
}

export default Router;
